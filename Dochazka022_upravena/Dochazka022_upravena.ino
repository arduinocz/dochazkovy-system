
#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>
#include <DS1307.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

LiquidCrystal_I2C lcd(0x27,20,4); 

// funkce - void -------------------------------------------------------------------------------
// funkce zápis byte
    void i2c_eeprom_write_byte( int deviceaddress, unsigned int eeaddress, byte data ) {
    int rdata = data;
    Wire.beginTransmission(deviceaddress);
    Wire.write((int)(eeaddress >> 8)); // MSB
    Wire.write((int)(eeaddress & 0xFF)); // LSB
    Wire.write(rdata);
    Wire.endTransmission();
    delay(10);
  }
// funkce čtení byte      
    byte i2c_eeprom_read_byte( int deviceaddress, unsigned int eeaddress ) {
    byte rdata = 0xFF;
    Wire.beginTransmission(deviceaddress);
    Wire.write((int)(eeaddress >> 8)); // MSB
    Wire.write((int)(eeaddress & 0xFF)); // LSB
    Wire.endTransmission();
    Wire.requestFrom(deviceaddress,1);
    if (Wire.available()) rdata = Wire.read();
    return rdata;
  }
  
// proměnné -------------------------------------------------------------------------------------------
byte adresa01_32; // low byte
byte adresa02_32; // high byte
byte adresa03_32; // low byte
byte adresa04_32; // high byte
byte adresa01_256; // low byte
byte adresa02_256; // high byte
byte adresa03_256; // low byte
byte adresa04_256; // high byte

int adresa32a;
int adresa32b;
int adresa256a;
int adresa256b;
int adresa;
int adresaEEPROM;

int s_den7;
int s_den;
int s_mesic;
int s_rok;
int s_hod;
int s_min;
int s_sec;

byte mesic1;
byte den1;
byte hodin1;
byte minut1;

byte aca=0;
byte aca2=0;
int rok;
byte dataRF[15]; //14
byte dataRF1[7]; //6
unsigned long dataRF2 = 0;
unsigned long dataRF3 = 0;
int rtc[7];
byte value;
byte sec=0;
byte sv_c=0;
byte rd_c=0;
byte sv_sum;
byte rd_sum;

byte hodiny[7];  //puv. hodnota 6, ukládání hodin vyhodnocení docházky č.1-6
byte minuty[7];  //puv. hodnota 6, ukládání minut vyhodnocení docházky č.1-6
byte pom3;
float hodiny_sum[32]; // 31
float hodiny_sum2;
int hodiny_sum_pom1;
int hodiny_sum_pom2;
int odprac_dnu;

byte pom1;
byte pom2;

String vyhledavani = "";
String vyhledavani2 = "";
String vyhledavani3 = "";

unsigned int prom_for1 = 0;
unsigned int prom_for2 = 0;
unsigned int prom_for3 = 0;
unsigned int address = 0;
// maximální adresy EEPROM pamětí
unsigned int EEPROM_in = 1010;
unsigned int EEPROM_32 = 4066;
unsigned int EEPROM_256 = 32700;
// adresy pamětí EEPROM
byte adr_32 = 0x50;
byte adr_256 = 0x52;
byte adrSVRD;

boolean podminka1 = true;  // podmínka, když true tak spusť uvodní stránku
boolean cip_nalezen = false;
boolean nulovani = true;
boolean pracujem = false;
boolean sv[32]; //31
boolean rd[32]; //31
boolean sv_p;
boolean rd_p;
boolean pom4;
boolean csv = false;

char prevod[4]; //3
char prevod2[3]; //2

int time_reset = 0;

byte mac[] = {0x11, 0xFE, 0xED, 0x03, 0xF4, 0xD7}; // nastavení MAC

//-------------------------------------------------
// IP přidělená správcem sítě: 192.168.1.111      // IP
// IP moje, testovací: 192.168.33.220         // IP
IPAddress ip(192, 168, 1, 111);              // IP
char HTML_IP[] = "http://192.168.1.111";     // IP
// JE nutno změnit taky v HTML_start[]!!!      // IP
//-------------------------------------------------

const char HTML_start[] = "<!DOCTYPE HTML><html><head><meta charset='UTF-8'><title>Docházkový systém</title></head><body><h1>&nbsp;<a href='http://192.168.1.111' title='Úvodní stránka'><u>DOCHÁZKOVÝ SYSTÉM</u></a></h1><SCRIPT>function passWord(){x=prompt('Prosím zadej PIN1',' '); if (x=='1234') {return true} else return false;} function date() {var datum=new Date();var r='?date='+datum.getDay()+'.';if (datum.getDate()<10){r+='0'} r+=datum.getDate()+'.';if (1+datum.getMonth()<10){r+='0'} r+= 1+datum.getMonth()+'.'+datum.getFullYear()+'.';if (datum.getHours()<10){r+='0'} r+=datum.getHours()+'.';if (datum.getMinutes()<10){r+='0'} r+=datum.getMinutes()+'.';if (datum.getSeconds()<10){r+='0'} r+=datum.getSeconds();window.location.href=r;}</SCRIPT>";
const char HTML_konec[] = "</body></html>";

EthernetServer server(80);

void setup() {
  //Serial.begin(115200);
  Wire.begin();
  Serial1.begin(9600);  // zač. komunikace s čtečkou čipů 
  Ethernet.begin(mac, ip);
  server.begin();
  lcd.init();// inicializuje displej
  lcd.backlight(); // zapne podsvětlení
  
//   RTC.stop(); // zastaví čas
//  RTC.set(DS1307_SEC,1); //nastaví sekundy
//  RTC.set(DS1307_MIN,14); // nastaví minuty
//  RTC.set(DS1307_HR,16); // nastaví hodiny
//  RTC.set(DS1307_DOW,4); // nastaví den v týdnu
//  RTC.set(DS1307_DATE,28); // nastaví den v měsíci
//  RTC.set(DS1307_MTH,1); // nastaví měsíc
//  RTC.set(DS1307_YR,10); // nastaví rok
//  RTC.start(); // spustí čas */

/* // manuální výmaz SV a ŘD
  for (prom_for1=0; prom_for1<3000; prom_for1++) {
  i2c_eeprom_write_byte(adr_32, prom_for1, 0);
  Serial.print(prom_for1);
  Serial.print(" ");
  Serial.println(i2c_eeprom_read_byte(adr_32, prom_for1));
}*/

}

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void loop()
{
   RTC.get(rtc,true); 
   char cas[9];  
   char datum[11];
   
   // auto reset --------------------------------------------------
   if ( (rtc[2] == 0) && (time_reset == 23) )   resetFunc();  //call reset  
   time_reset = rtc[2];
   // -------------------------------------------------------------------
   
   sprintf(cas, "%02d:%02d:%02d", rtc[2],rtc[1],rtc[0]);
   sprintf(datum, "%02d.%02d.%02d", rtc[4],rtc[5],rtc[6]);
   String den_v_tydnu = "";
   if (rtc[3]==1) den_v_tydnu = "Pondeli";
   if (rtc[3]==2) den_v_tydnu = "Utery  ";
   if (rtc[3]==3) den_v_tydnu = "Streda ";
   if (rtc[3]==4) den_v_tydnu = "Ctvrtek";
   if (rtc[3]==5) den_v_tydnu = "Patek  ";
   if (rtc[3]==6) den_v_tydnu = "Sobota ";
   if (rtc[3]==7) den_v_tydnu = "Nedele ";
   lcd.setCursor(0,0);
   lcd.print(cas);
   lcd.setCursor(0,1);
   lcd.print(den_v_tydnu);
   lcd.setCursor(10,1);
   lcd.print(datum);

  if (((rtc[2]>5 && rtc[2]<14) || (rtc[2]==14 && rtc[1]<30))  &&  (sec != rtc[0] && pracujem==false)) {
     lcd.setCursor(0,3);
     lcd.print("!! Pracovni doba !!");
     sec=rtc[0];
     pracujem=true;
  }
  else if ( (rtc[2]>14 && rtc[2]<20 )  &&  (sec != rtc[0] && pracujem==false)) {
     lcd.setCursor(0,3);
     lcd.print("    ! Prescas !    ");
     sec=rtc[0];
     pracujem=true;
  }
  else if (sec != rtc[0]) {
     lcd.setCursor(0,3);
     lcd.print("                    ");
     sec=rtc[0];
     pracujem=false;
   }   
 

// Snímání kódu z čipu ------------------------------------------------------------
     if (Serial1.available()) { // když je něco v bufferu
       lcd.setCursor(5,0);
       lcd.print("   ");
       cip_nalezen = false;
       delay(200); // počkej, až jsou přenesena všechna data do bufferu
       for (aca=0; aca<14; aca++) dataRF[aca]=Serial1.read();
      if (dataRF[0]==2 && dataRF[13]==3) { 
       for (aca=0; aca<10; aca++) {
         //lcd.write(dataRF[aca+1]);
         }
      }
      else {
        lcd.setCursor(0,3);
        lcd.print("Chyba cteni - znovu!");
      }
// --- hlednání kódu v EEPROM a přiřazení adresy (Jména a čísla zaměstnanace)
   for (aca=0; aca<6; aca++) {
     if (dataRF[aca+5] == 48) dataRF1[aca] = 0;
     if (dataRF[aca+5] == 49) dataRF1[aca] = 1;
     if (dataRF[aca+5] == 50) dataRF1[aca] = 2;
     if (dataRF[aca+5] == 51) dataRF1[aca] = 3;
     if (dataRF[aca+5] == 52) dataRF1[aca] = 4;
     if (dataRF[aca+5] == 53) dataRF1[aca] = 5;
     if (dataRF[aca+5] == 54) dataRF1[aca] = 6;
     if (dataRF[aca+5] == 55) dataRF1[aca] = 7;
     if (dataRF[aca+5] == 56) dataRF1[aca] = 8;
     if (dataRF[aca+5] == 57) dataRF1[aca] = 9;
     if (dataRF[aca+5] == 65) dataRF1[aca] = 10;
     if (dataRF[aca+5] == 66) dataRF1[aca] = 11;
     if (dataRF[aca+5] == 67) dataRF1[aca] = 12;
     if (dataRF[aca+5] == 68) dataRF1[aca] = 13;
     if (dataRF[aca+5] == 69) dataRF1[aca] = 14;
     if (dataRF[aca+5] == 70) dataRF1[aca] = 15;
   }
  dataRF2 = dataRF1[0]*long(1048576) + dataRF1[1]*long(65536) + dataRF1[2]*long(4096) + dataRF1[3]*long(256) + dataRF1[4]*16 + dataRF1[5];
  
   for (prom_for1=0; prom_for1 < EEPROM_in; prom_for1=prom_for1+34){
     for (prom_for2=26; prom_for2 <=32; prom_for2++) {
       address = prom_for1 + prom_for2;
       value = EEPROM.read(address) - 48;
       if (value>10) {
         dataRF[prom_for2-26] = 0;
       }
       else {
         dataRF[prom_for2-26] = value;
       }
     }
     dataRF3 = dataRF[0]*long(1000000) + dataRF[1]*long(100000) + dataRF[2]*long(10000) + dataRF[3]*long(1000) + dataRF[4]*100 + dataRF[5]*(10) + dataRF[6];
     if (dataRF3 == dataRF2) {
       if (cip_nalezen != true) {
       cip_nalezen = true;
       lcd.setCursor(0,2);
       for (prom_for2=3; prom_for2 <=22; prom_for2++) {
        address = prom_for1 + prom_for2;
        value = EEPROM.read(address);
        if (value<65 || value>122 || (value>90 && value<97)) {
          lcd.print(" ");
        }
        else {
          lcd.write(value);
        }
      }
// ukládání do EEPROM ----------------------------------------------------------------------------------------------
adresa01_32 = i2c_eeprom_read_byte(0x50, 4090);
adresa02_32 = i2c_eeprom_read_byte(0x50, 4091);
adresa03_32 = i2c_eeprom_read_byte(0x50, 4092);
adresa04_32 = i2c_eeprom_read_byte(0x50, 4093);
adresa32a = ((adresa01_32 << 0) & 0xFF) + ((adresa02_32 << 8) & 0xFF00);
adresa32b = ((adresa03_32 << 0) & 0xFF) + ((adresa04_32 << 8) & 0xFF00);

adresa01_256 = i2c_eeprom_read_byte(0x52, 32750);
adresa02_256 = i2c_eeprom_read_byte(0x52, 32751);
adresa03_256 = i2c_eeprom_read_byte(0x52, 32752);
adresa04_256 = i2c_eeprom_read_byte(0x52, 32753);
adresa256a = ((adresa01_256 << 0) & 0xFF) + ((adresa02_256 << 8) & 0xFF00);
adresa256b = ((adresa03_256 << 0) & 0xFF) + ((adresa04_256 << 8) & 0xFF00);

if (adresa32a==adresa32b && adresa256a==adresa256b && adresa32a==adresa256b) {
  adresa = adresa32a;
  // zápis dat ------------------------------------------------------------------
  i2c_eeprom_write_byte(0x52, adresa, EEPROM.read(prom_for1));     // číslo pracovníka
  i2c_eeprom_write_byte(0x52, adresa+1, EEPROM.read(prom_for1+1)); // číslo pracovníka
  i2c_eeprom_write_byte(0x52, adresa+2, EEPROM.read(prom_for1+2)); // číslo pracovníka
  i2c_eeprom_write_byte(0x52, adresa+3, rtc[4]);       // den
  i2c_eeprom_write_byte(0x52, adresa+4, rtc[5]);       // měsíc
  i2c_eeprom_write_byte(0x52, adresa+5, rtc[6]-2000);  // rok
  i2c_eeprom_write_byte(0x52, adresa+6, rtc[3]);       // den v týdnu
  i2c_eeprom_write_byte(0x52, adresa+7, rtc[2]);  // hodin
  i2c_eeprom_write_byte(0x52, adresa+8, rtc[1]);  // minut
  lcd.setCursor(0,3);
  lcd.print("  --- Zapsano ---   ");
  // zápis nové adresy ----------------------------------------------------------
  adresa = adresa+9;
  if (adresa > EEPROM_256) adresa = 0;  // vynulování adresy na max. paměti
  adresa01_32 = ((adresa >> 0) & 0xFF);   // low byte
  adresa02_32 = ((adresa >> 8) & 0xFF);    // high byte
  adresa01_256 = adresa01_32;    // low byte
  adresa02_256 = adresa02_32;    // high byte
  i2c_eeprom_write_byte(0x50, 4090, adresa01_32);  // low byte
  i2c_eeprom_write_byte(0x50, 4091, adresa02_32);   // high byte
  i2c_eeprom_write_byte(0x50, 4092, adresa01_32);  // low byte
  i2c_eeprom_write_byte(0x50, 4093, adresa02_32);   // high byte
  i2c_eeprom_write_byte(0x52, 32750, adresa01_256);  // low byte
  i2c_eeprom_write_byte(0x52, 32751, adresa02_256);  // high byte
  i2c_eeprom_write_byte(0x52, 32752, adresa01_256);  // low byte
  i2c_eeprom_write_byte(0x52, 32753, adresa02_256);  // high byte
  // ----------------------------------------------------------------------------
  tone(7, 2400, 600);
}
else if (adresa32a != adresa32b) {
  lcd.setCursor(0,3);
  lcd.print("Chyba EEPROM 32!    ");
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(2000);
}
else if (adresa256a != adresa256b) {
  lcd.setCursor(0,3);
  lcd.print("Chyba EEPROM 256!   ");
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(2000);
}
else if (adresa32a!=adresa256a || adresa32a!=adresa256b || adresa32b!=adresa256a || adresa32b!=adresa256b) {
  lcd.setCursor(0,3);
  lcd.print("Chyba EEPROM!       ");
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(300);
  tone(7, 1600, 150);
  delay(2000);
}
 
// -----------------------------------------------------------------------------------------------------------------       
     
     }
    else {
      
      lcd.setCursor(0,2);
      lcd.print("Duplicitni zaznam!");
      lcd.setCursor(0,3);
      lcd.print("Informuj spravce!   ");
      tone(7, 1600, 150);
      delay(300);
      tone(7, 1600, 150);
      delay(300);
      tone(7, 1600, 150);
      delay(2000);
    } 
     }
     
   }
       if (cip_nalezen == false) {
         lcd.setCursor(10,0);
         lcd.print("000");
         lcd.print(dataRF2);
         lcd.setCursor(0,2);
         lcd.print("Nerozpoznan CIP!");
         lcd.setCursor(0,3);
         lcd.print("Zkus znovu prilozit!");
         tone(7, 1600, 150);
         delay(300);
         tone(7, 1600, 150);
         delay(300);
         tone(7, 1600, 150);
       }
       
       delay(3000);
       lcd.setCursor(10,0);
       lcd.print("          ");
       lcd.setCursor(0,2);
       lcd.print("                    ");
       lcd.setCursor(0,3);
       lcd.print("                    ");
       while(Serial1.available()) Serial1.read();
  }


// Ethernet -------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
   podminka1 = true;
   EthernetClient client = server.available();
   String buffer = "";
   while (client.connected()) {
  
    // čti data od něj dokud nenarazíš na znak nového řádku 
     if (client.available()) {
        char c = client.read();
        buffer = buffer + c;
         if (c == '\n') {

// val začátek ----------------------------------------------------------
       for (prom_for3=0; prom_for3 < EEPROM_in; prom_for3 = prom_for3+34) {
       sprintf(prevod, "%03d", prom_for3);

// val1 ----------------------------------------------------------          
       vyhledavani = "";
       vyhledavani = vyhledavani + "adr=";
       vyhledavani = vyhledavani + prevod;
       vyhledavani = vyhledavani + "+val=1";
         if(buffer.indexOf(vyhledavani)>=0) {
           podminka1 = false; 
           int pozice_buffer = buffer.indexOf("cislo=");
           if(pozice_buffer >= 0) {
             client.println(HTML_start); // úvodní seqence
             int q = pozice_buffer+6;
             for (prom_for1=0; prom_for1<3; prom_for1++) {
               EEPROM.write(prom_for1+prom_for3, 32);
               delay(5);
             }
             prom_for1 = 0;
             while (prom_for1<3) {
               value = buffer[q + prom_for1];
               if (value == 32) {
                 prom_for1 = 3;
               }
               else {
               EEPROM.write(prom_for3+prom_for1, value);
               delay(5);
               }
               prom_for1++;
             }
            client.println("Zapisuji!");
            client.print(F("<meta http-equiv=\"refresh\" content=\"1;url="));
            client.print(HTML_IP);
            client.println("\">");
           } 
           else {
           
          client.println(HTML_start); // úvodní seqence
          client.println(F("<form method='get' onsubmit='return passWord()'><label for='cislo'>Zadej 3-ciferné číslo pracovníka, odeslání prázdného formuláře vymaže záznam:&nbsp;</label><input type='text' size='6' autocomplete='off' id='cislo' maxlength='3' name='cislo'><input type='submit' value='Odeslat'></form>"));
           }
          } 

// val2 ----------------------------------------------------------- 
       vyhledavani2 = "";
       vyhledavani2 = vyhledavani2 + "adr=";
       vyhledavani2 = vyhledavani2 + prevod;
       vyhledavani2 = vyhledavani2 + "+val=2";
           if(buffer.indexOf(vyhledavani2)>=0) {
           podminka1 = false; 
           int pozice_buffer = buffer.indexOf("jmeno=");
           if(pozice_buffer >= 0) {
             client.println(HTML_start); // úvodní seqence
             int q = pozice_buffer+6;
             for (prom_for1=0; prom_for1<20; prom_for1++) {
               EEPROM.write(prom_for1+prom_for3+3, 32);
               delay(5);
             }
             prom_for1 = 0;
             while (prom_for1<20) {
               value = buffer[q + prom_for1];
               if (value == 32) {
                 prom_for1 = 20;
               }
               else {
               EEPROM.write(prom_for3+3+prom_for1, value);
               delay(5);
               }
               prom_for1++;
             }
            client.println("Zapisuji!");
            client.print(F("<meta http-equiv=\"refresh\" content=\"1;url="));
            client.print(HTML_IP);
            client.println("\">");
           } 
           else {
           
          client.println(HTML_start); // úvodní seqence
          client.println(F("<form method='get' onsubmit='return passWord()'><label for='jmeno'>Zadej \"Prijmeni Jmeno\" <font color='red'>(bez diakritiky!)</font>, odeslání prázdného formuláře vymaže záznam:&nbsp;</label><input type='text' size='25' autocomplete='off' id='jmeno' maxlength='20' name='jmeno'><input type='submit' value='Odeslat'></form>"));
           }
          }
          
// val3 ----------------------------------------------------------- 
       vyhledavani3 = "";
       vyhledavani3 = vyhledavani3 + "adr=";
       vyhledavani3 = vyhledavani3 + prevod;
       vyhledavani3 = vyhledavani3 + "+val=3";
           if(buffer.indexOf(vyhledavani3)>=0) {
             podminka1 = false; 
             
             int pozice_buffer = buffer.indexOf("kod=");
             if(pozice_buffer >= 0) {
               client.println(HTML_start); // úvodní seqence
               int q = pozice_buffer+4;
               for (prom_for1=0; prom_for1<10; prom_for1++) {
                 EEPROM.write(prom_for1+prom_for3+23, 32);
                 delay(5);
               }
               prom_for1 = 0;
               while (prom_for1<10) {
                 value = buffer[q + prom_for1];
                 if (value == 32) {
                   prom_for1 = 10;
                   }
                 else {
                   EEPROM.write(prom_for3+23+prom_for1, value);
                   delay(5);
                 }
                 prom_for1++;
              }
             client.println("Zapisuji!");
             client.print(F("<meta http-equiv=\"refresh\" content=\"1;url="));
             client.print(HTML_IP);
             client.println("\">");
             }
             else {
               client.println(HTML_start); // úvodní seqence
               client.println(F("<form method='get' onsubmit='return passWord()'><label for='kod'>Zadej <font color='blue'>desetimístný</font> kód čipu, odeslání prázdného formuláře vymaže záznam:&nbsp;</label><input type='text' size='15' autocomplete='off' id='kod' maxlength='10' name='kod'><input type='submit' value='Odeslat'></form>"));
             }
             
             
             
           }          

//--------------------------------------------------------          
       }
// val konec ----------------------------------------------------------


// Prohlížení docházky - html---------------------------------------------------
if(buffer.indexOf("html")>=0) {
             podminka1 = false; 
 if (buffer.indexOf("day")>=0) {
  if (buffer.indexOf("cas")>=0) {
    client.println(HTML_start); // úvodní seqence
    // zapisuj ------
    if (buffer[26]>47 && buffer[26]<58 && buffer[27]>47 && buffer[27]<58 && buffer[33]>47 && buffer[33]<58 && buffer[34]>47 && buffer[34]<58 && buffer[41]==56 && buffer[42]==57 && buffer[43]==49 && buffer[44]==51) { // PIN2 "4321" buffer 41-44
      hodin1 = (buffer[26]-'0')*10 + (buffer[27]-'0');
      minut1 = (buffer[33]-'0')*10 + (buffer[34]-'0');
      if (hodin1>5 && hodin1<21 && (minut1==00 || minut1==30)) {
        // ukládání do EEPROM ----------------------------------------------------------------------------------------------
        adresa01_32 = i2c_eeprom_read_byte(0x50, 4090);
        adresa02_32 = i2c_eeprom_read_byte(0x50, 4091);
        adresa03_32 = i2c_eeprom_read_byte(0x50, 4092);
        adresa04_32 = i2c_eeprom_read_byte(0x50, 4093);
        adresa32a = ((adresa01_32 << 0) & 0xFF) + ((adresa02_32 << 8) & 0xFF00);
        adresa32b = ((adresa03_32 << 0) & 0xFF) + ((adresa04_32 << 8) & 0xFF00);

        adresa01_256 = i2c_eeprom_read_byte(0x52, 32750);
        adresa02_256 = i2c_eeprom_read_byte(0x52, 32751);
        adresa03_256 = i2c_eeprom_read_byte(0x52, 32752);
        adresa04_256 = i2c_eeprom_read_byte(0x52, 32753);
        adresa256a = ((adresa01_256 << 0) & 0xFF) + ((adresa02_256 << 8) & 0xFF00);
        adresa256b = ((adresa03_256 << 0) & 0xFF) + ((adresa04_256 << 8) & 0xFF00);

        if (adresa32a==adresa32b && adresa256a==adresa256b && adresa32a==adresa256b) {
          adresa = adresa32a;
          // zápis dat ------------------------------------------------------------------
          i2c_eeprom_write_byte(0x52, adresa, buffer[5]);     // číslo pracovníka
          i2c_eeprom_write_byte(0x52, adresa+1, buffer[6]); // číslo pracovníka
          i2c_eeprom_write_byte(0x52, adresa+2, buffer[7]); // číslo pracovníka
          i2c_eeprom_write_byte(0x52, adresa+3, den1);       // den
          i2c_eeprom_write_byte(0x52, adresa+4, mesic1);       // měsíc
          i2c_eeprom_write_byte(0x52, adresa+5, rtc[6]-2000);  // rok
          if (den1==rtc[4] && mesic1==rtc[5]) {
            i2c_eeprom_write_byte(0x52, adresa+6, rtc[3]);       // den v týdnu
          }
          else i2c_eeprom_write_byte(0x52, adresa+6, 0);       // den v týdnu
          i2c_eeprom_write_byte(0x52, adresa+7, hodin1);  // hodin
          i2c_eeprom_write_byte(0x52, adresa+8, minut1);  // minut
          // zápis nové adresy ----------------------------------------------------------
          adresa = adresa+9;
          if (adresa > EEPROM_256) adresa = 0;  // vynulování adresy na max. paměti
          adresa01_32 = ((adresa >> 0) & 0xFF);   // low byte
          adresa02_32 = ((adresa >> 8) & 0xFF);    // high byte
          adresa01_256 = adresa01_32;    // low byte
          adresa02_256 = adresa02_32;    // high byte
          i2c_eeprom_write_byte(0x50, 4090, adresa01_32);  // low byte
          i2c_eeprom_write_byte(0x50, 4091, adresa02_32);   // high byte
          i2c_eeprom_write_byte(0x50, 4092, adresa01_32);  // low byte
          i2c_eeprom_write_byte(0x50, 4093, adresa02_32);   // high byte
          i2c_eeprom_write_byte(0x52, 32750, adresa01_256);  // low byte
          i2c_eeprom_write_byte(0x52, 32751, adresa02_256);  // high byte
          i2c_eeprom_write_byte(0x52, 32752, adresa01_256);  // low byte
          i2c_eeprom_write_byte(0x52, 32753, adresa02_256);  // high byte
          // ----------------------------------------------------------------------------
        }
        client.println(F("<br><br><font color='red'><b>Zapsáno!<b></font>"));
      }
      else client.println(F("<br><br><font color='blue'><b>Uložení selhalo!<b></font>"));
    }
    else client.println(F("<br><br><font color='blue'><b>Uložení selhalo!<b></font>"));
    client.print("<script>window.location.replace('");
    client.print(HTML_IP);
    client.print("/");
    client.write(buffer[5]);
    client.write(buffer[6]);
    client.write(buffer[7]);
    client.print("_");
    client.write(buffer[9]);
    client.write(buffer[10]);
    client.println(".html');</script>");
  }
  else {
   client.println(HTML_start); // úvodní seqence
   client.println(F("<br><font color='red'><b><u>FUNKCE mimořádného zadání příchodu/odchodu</u></b><br><br><b>Pokud je zadán čas (datum) dřívější, než je poslední čas (datum) načtený čipem, výpočet odpracovaných hodin toho dne (a pak i v součtu za měsíc) bude 0!</b></font><br><br><br>"));
   client.print(F("<b><u>Bude se zapisovat:</u></b><br><br>"));
   client.print("<b>Pracovník č.: </b>");
   client.write(buffer[5]);
   client.write(buffer[6]);
   client.write(buffer[7]);
   mesic1 = (buffer[9]-'0')*10 + (buffer[10]-'0'); // výpočet měsíce
   den1 = (buffer[20]-'0')*10 + (buffer[21]-'0'); // den
   client.print("<br><b>Datum: </b>");
   client.print(den1);
   client.print(". ");
   client.print(mesic1);
   client.print(". ");
   client.print(rtc[6]);
   client.print("<br><br><b>Čas: </b>");
   client.print(F("<form method='get'><select name='day"));
   if (den1<10) client.print("0");
   client.print(den1);
   client.println(F("cas'><option value='06' selected='selected'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option></select><select name='min'><option value='00' selected='selected'>00</option><option value='30'>30</option></select><br><br><label for='heslo'>PIN2 </label><input type='password' name='pass' id='heslo'><br><br><input type='submit' value='Zapiš!'></form>"));
  }
 }
 else {
   // vyhledání pracovníka + měsíce v EEPROM256 a makopírování do EEPROM Arduino --------------------------------------------
   adresaEEPROM=2048;  // výchozí nultá pozice
   adresa01_32 = i2c_eeprom_read_byte(0x50, 4090);
   adresa02_32 = i2c_eeprom_read_byte(0x50, 4091);
   adresa32a = ((adresa01_32 << 0) & 0xFF) + ((adresa02_32 << 8) & 0xFF00);
   aca = (buffer[9]-'0')*10 + (buffer[10]-'0'); // výpočet měsíce
   mesic1 = aca;
   sv_p = false;
   rd_p = false;
   if(buffer.indexOf("rd")>=0) {
     rd_c = (buffer[19]-'0')*10 + (buffer[20]-'0'); // výpočet dne dovolené
     rd_p = true;
   }
   if(buffer.indexOf("sv")>=0) {
     sv_c = (buffer[19]-'0')*10 + (buffer[20]-'0'); // výpočet dne svátku
     sv_p = true;
   }
   
   if ((rtc[5]-aca)<0) {
     rok = rtc[6]-2000-1;
   }
   else rok = rtc[6]-2000;
   nulovani = true;
   for (prom_for1=0; prom_for1<EEPROM_256; prom_for1=prom_for1+9) {
     if ((adresa32a+prom_for1)<EEPROM_256) {
       adresa = (adresa32a+prom_for1);
     }
     else {
       if (nulovani == true) {
         nulovani = false;
         adresa = -9;
       }
       adresa = adresa+9;
     }
     if (i2c_eeprom_read_byte(0x52, adresa)==buffer[5]) {
       if (i2c_eeprom_read_byte(0x52, adresa+1)==buffer[6]) {
         if (i2c_eeprom_read_byte(0x52, adresa+2)==buffer[7]) {
           if (i2c_eeprom_read_byte(0x52, adresa+4)==aca) {
             if (i2c_eeprom_read_byte(0x52, adresa+5)==rok) {
               EEPROM.write(adresaEEPROM+3, i2c_eeprom_read_byte(0x52, adresa+3));
               EEPROM.write(adresaEEPROM+6, i2c_eeprom_read_byte(0x52, adresa+6));
               EEPROM.write(adresaEEPROM+7, i2c_eeprom_read_byte(0x52, adresa+7));
               EEPROM.write(adresaEEPROM+8, i2c_eeprom_read_byte(0x52, adresa+8));
               adresaEEPROM=adresaEEPROM+9;
               if (adresaEEPROM > 4000) adresaEEPROM = adresaEEPROM - 9;
             }
           }
         }
       }
     }
   }
   //--------------------------------------------------------------------------------------------------------------------------   
             client.println(HTML_start); // úvodní seqence
             client.print(adresa32a);
             client.println("<br>");
             client.println(F("<table align='left' border='1' cellpadding='6' cellspacing='0'>"));
             client.print(F("<tr><th bgcolor='cyan'>"));
             for (prom_for3=0; prom_for3 < EEPROM_in; prom_for3 = prom_for3+34) {
               if (EEPROM.read(prom_for3) == buffer[5]) {
                 if (EEPROM.read(prom_for3 + 1) == buffer[6]) {
                   if (EEPROM.read(prom_for3 + 2) == buffer[7]) {
                     
                     for (prom_for1=0; prom_for1<31; prom_for1++) {  // zpracování ŘD a SV
                       adrSVRD = i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4));
                       if (rd_p==true && rd_c==prom_for1+1) {
                         rd[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)));
                         bitWrite(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)), !rd[prom_for1]);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4), adrSVRD);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7, 192 | (i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7))  ); // zapsání 8a7 bite 8 bajtu log 1 - do měsíce se zapisovalo
                         rd_p=false;
                       }
                       if (sv_p==true && sv_c==prom_for1+1) {
                         sv[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1 );
                         bitWrite(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1, !sv[prom_for1]);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4), adrSVRD);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7, 192 | (i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7))  ); // zapsání 8a7 bite 8 bajtu log 1 - do měsíce se zapisovalo
                         sv_p=false;
                       }
                       rd[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)));
                       sv[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1 );
                     }
                     for (prom_for2 = 0; prom_for2 < 20; prom_for2++) {
                       value = EEPROM.read(prom_for3 + prom_for2 + 3);
                       if (value<65 || value>122 || (value>90 && value<97)) {
                          client.write("&nbsp;");
                        }
                       else {
                         client.write(value);
                        }
                     }
                     client.print("č.&nbsp;");
                     client.write(buffer[5]);
                     client.write(buffer[6]);
                     client.write(buffer[7]);
                   }
                 }
               }
             }
             client.print(F("</th><th bgcolor='violet'>Měsíc - "));
             client.print(buffer[9]);
             client.print(buffer[10]);
             client.print("/");
             client.print(rok+2000);
             client.println(F("</th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th><th bgcolor='violet'></th></tr>"));
             client.println(F("<tr><th bgcolor='yellow'>Den</th><th bgcolor='yellow'>Den v týdnu</th><th bgcolor='yellow'>Příchod</th><th bgcolor='yellow'>Odchod</th><th bgcolor='yellow'>Příchod</th><th bgcolor='yellow'>Odchod</th><th bgcolor='yellow'>Příchod</th><th bgcolor='yellow'>Odchod</th><th bgcolor='yellow'>Hodin</th><th bgcolor='yellow'>Řd</th><th bgcolor='yellow'>Sv</th><th bgcolor='yellow'>Přesčas</th></tr>"));
             
             for (prom_for1 = 1; prom_for1 < 32; prom_for1++) { 
               
               client.print("<tr><td align='center'>");
               //if (mesic1==rtc[5]) {
                 client.print("<a  href='?day");
                 if (prom_for1<10) client.print("0");
                 client.print(prom_for1);
                 client.print("' onclick='return passWord();return true;'>");
               //}
               client.print(prom_for1);
               client.print(".");
               //if (mesic1==rtc[5]) client.print("</a>");
               client.print("</td>");
               
               pom3 = 0;
              // den v týdnu a časy  ------------------------------------------            
               pom1 = 1;
               for (prom_for2 = 2048; prom_for2 < adresaEEPROM; prom_for2 = prom_for2+9) {
                 pom2 = EEPROM.read(prom_for2+3);
                 if (pom2 == prom_for1 && (pom1>2 && pom1<8)) {
                   pom1++;
                   client.print("<td>");
                   hodiny[pom3] = EEPROM.read(prom_for2+7);
                   if (hodiny[pom3]<10) client.print("0");
                   client.print(hodiny[pom3]);
                   client.print(":");
                   minuty[pom3] = EEPROM.read(prom_for2+8);
                   if (minuty[pom3]<10) client.print("0");
                   client.print(minuty[pom3]);
                   client.print("</td>");
                   pom3++;
                 }
                 else if (pom2 == prom_for1 && pom1 == 1) {
                   pom1=pom1+2;
                   client.print("<td align='center'>");
                   aca = EEPROM.read(prom_for2+6);
                   if (aca==1) den_v_tydnu = "Pondělí";
                   if (aca==2) den_v_tydnu = "Úterý";
                   if (aca==3) den_v_tydnu = "Středa";
                   if (aca==4) den_v_tydnu = "Čtvrtek";
                   if (aca==5) den_v_tydnu = "Pátek";
                   if (aca==6) den_v_tydnu = "Sobota";
                   if (aca==7) den_v_tydnu = "Neděle";
                   if (aca==0) den_v_tydnu = "";
                   client.print(den_v_tydnu);
                   client.print("</td>");
                   client.print("<td>");
                   hodiny[pom3] = EEPROM.read(prom_for2+7);
                   if (hodiny[pom3]<10) client.print("0");
                   client.print(hodiny[pom3]);
                   client.print(":");
                   minuty[pom3] = EEPROM.read(prom_for2+8);
                   if (minuty[pom3]<10) client.print("0");
                   client.print(minuty[pom3]);
                   client.print("</td>");
                   pom3++;
                   }
               }
               for (prom_for2=0; prom_for2<(8-pom1); prom_for2++) {
                 client.print("<td></td>");
                 hodiny[pom3] = 0;
                 minuty[pom3] = 0;
                 pom3++;
               }
              //Součet hodin a výpočet přesčasu ---------------------------------------------------------------------
              for (pom3=0; pom3<6; pom3++) {
                if (hodiny[pom3]<6 && hodiny[pom3]>0) {
                  hodiny[pom3] = 6;
                  minuty[pom3] = 0;
                }
                if ((pom3 % 2)==0) {  
                  if (minuty[pom3]>0 && minuty[pom3]<31) {
                    minuty[pom3] = 30;
                  }
                  if (minuty[pom3]>30) {
                    minuty[pom3] = 0;
                    hodiny[pom3] = hodiny[pom3]+1;
                  }
                }
                if ((pom3 % 2)!=0) {  
                  if (minuty[pom3]>0 && minuty[pom3]<30) {
                    minuty[pom3] = 0;
                  }
                  if (minuty[pom3]>30) {
                    minuty[pom3] = 30;
                  }
                }
              }
              hodiny_sum[prom_for1-1]=0;
              for (pom3=0; pom3<6; pom3=pom3+2) {
                if (hodiny[pom3+1]>=hodiny[pom3]) {
                 if (minuty[pom3]==minuty[pom3+1]) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]);
                 }
                 else if ( (minuty[pom3]==30 && minuty[pom3+1]==0) && (hodiny[pom3+1]>hodiny[pom3]) ) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]-0.5);
                 }
                 else if( (minuty[pom3]==30 && minuty[pom3+1]==0) && (hodiny[pom3+1]==hodiny[pom3]) ) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + 0;
                 }
                 else if (minuty[pom3]==0 && minuty[pom3+1]==30) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]+0.5);
                 }
                }
               }
              if (hodiny_sum[prom_for1-1]>4.5) hodiny_sum[prom_for1-1]=hodiny_sum[prom_for1-1]-0.5;
              if (hodiny_sum[prom_for1-1]>10.5) hodiny_sum[prom_for1-1]=hodiny_sum[prom_for1-1]-0.5;
              client.print("<td>");
              if (hodiny_sum[prom_for1-1] != 0) client.print(hodiny_sum[prom_for1-1],1); // hodin
              client.print("</td><td align='center'><a href=\"javascript: window.location.replace('");
              client.print(HTML_IP);
              client.print("/");
              for (pom3=0; pom3<11; pom3++) {
                client.print(buffer[pom3 + 5]);
              }
              client.print("/rd");
              if (prom_for1<10) client.print("0");
              client.print(prom_for1);
              client.print("');\" onclick='return passWord();return true;'>");
              if (rd[prom_for1-1] == 1) {  // zobrazení ŘD
                client.print("ŘD");
              }
              else client.print("&nbsp;");
              client.print("</a></td><td align='center'><a href=\"javascript: window.location.replace('");
              client.print(HTML_IP);
              client.print("/");
              for (pom3=0; pom3<11; pom3++) {
                client.print(buffer[pom3 + 5]);
              }
              client.print("/sv");
              if (prom_for1<10) client.print("0");
              client.print(prom_for1);
              client.print("');\" onclick='return passWord();return true;'>");
              if (sv[prom_for1-1] == 1) {  // zobrazení ŘD
                client.print("SV");
              }
              else client.print("&nbsp;");
              client.print("</a></td><td>");
              if (hodiny_sum[prom_for1-1]>8 && rd[prom_for1-1]==false) client.print(hodiny_sum[prom_for1-1]-8,1); // přesčas
              if (hodiny_sum[prom_for1-1]>0 && rd[prom_for1-1]==true) client.print(hodiny_sum[prom_for1-1],1); // přesčas
              client.println("</td></tr>");
             }
             client.print(F("<tr><td align='center' bgcolor='yellow'><b>Odprac. dnů: "));
             odprac_dnu=0;
             hodiny_sum2=0;
             for (prom_for1=0; prom_for1<31; prom_for1++) {
               hodiny_sum2 = hodiny_sum2 + hodiny_sum[prom_for1];
               if (hodiny_sum[prom_for1] != 0 && rd[prom_for1]==false) odprac_dnu++;
             }
             client.print(odprac_dnu);
             client.print(F("</b></td><td align='center' bgcolor='yellow'><b>Stand. hodin: "));
             client.print(odprac_dnu*8);
             client.print(F("</b></td><td bgcolor='yellow'></td><td bgcolor='yellow'></td><td bgcolor='yellow'></td><td bgcolor='yellow'></td><td bgcolor='yellow'></td><td bgcolor='yellow'></td></td><td bgcolor='yellow'><b>Suma: "));
             client.print(hodiny_sum2,1);
             client.print(F("</b></td><td bgcolor='yellow'><b>Řd: "));
             rd_sum=0;
             sv_sum=0;
             for (prom_for1=0; prom_for1<31; prom_for1++) {
               rd_sum = rd_sum + rd[prom_for1];
               sv_sum = sv_sum + sv[prom_for1];
             }
             client.print(rd_sum);
             client.print(F("</b></td><td bgcolor='yellow'><b>Sv: "));
             client.print(sv_sum);
             client.print(F("</b></td><td bgcolor='yellow'><b>Přesčas: "));
             client.print(hodiny_sum2-(8*odprac_dnu),1);
             client.println("</b></td></tr>");
             client.println("</table>");
 }
}          


// Prohlížení docházky - csv ---------------------------------------------------
if(buffer.indexOf("csv")>=0) {
   podminka1 = false; 
   csv = true;
   // ****************************************************
   // vyhledání pracovníka + měsíce v EEPROM256 a makopírování do EEPROM Arduino --------------------------------------------
   adresaEEPROM=2048;  // výchozí nultá pozice
   adresa01_32 = i2c_eeprom_read_byte(0x50, 4090);
   adresa02_32 = i2c_eeprom_read_byte(0x50, 4091);
   adresa32a = ((adresa01_32 << 0) & 0xFF) + ((adresa02_32 << 8) & 0xFF00);
   
   // ?????????????
   aca = (buffer[9]-'0')*10 + (buffer[10]-'0'); // výpočet měsíce
   mesic1 = aca;
   sv_p = false;
   rd_p = false;
   if(buffer.indexOf("rd")>=0) {
     rd_c = (buffer[19]-'0')*10 + (buffer[20]-'0'); // výpočet dne dovolené
     rd_p = true;
   }
   if(buffer.indexOf("sv")>=0) {
     sv_c = (buffer[19]-'0')*10 + (buffer[20]-'0'); // výpočet dne svátku
     sv_p = true;
   }
   
   if ((rtc[5]-aca)<0) {
     rok = rtc[6]-2000-1;
   }
   else rok = rtc[6]-2000;
   // ???????????????????
   
   nulovani = true;
   for (prom_for1=0; prom_for1<EEPROM_256; prom_for1=prom_for1+9) {
     if ((adresa32a+prom_for1)<EEPROM_256) {
       adresa = (adresa32a+prom_for1);
     }
     else {
       if (nulovani == true) {
         nulovani = false;
         adresa = -9;
       }
       adresa = adresa+9;
     }
   if (i2c_eeprom_read_byte(0x52, adresa)==buffer[5]) {
       if (i2c_eeprom_read_byte(0x52, adresa+1)==buffer[6]) {
         if (i2c_eeprom_read_byte(0x52, adresa+2)==buffer[7]) {
           if (i2c_eeprom_read_byte(0x52, adresa+4)==aca) {
             if (i2c_eeprom_read_byte(0x52, adresa+5)==rok) {
               EEPROM.write(adresaEEPROM+3, i2c_eeprom_read_byte(0x52, adresa+3));
               EEPROM.write(adresaEEPROM+6, i2c_eeprom_read_byte(0x52, adresa+6));
               EEPROM.write(adresaEEPROM+7, i2c_eeprom_read_byte(0x52, adresa+7));
               EEPROM.write(adresaEEPROM+8, i2c_eeprom_read_byte(0x52, adresa+8));
               adresaEEPROM=adresaEEPROM+9;
               if (adresaEEPROM > 4000) adresaEEPROM = adresaEEPROM - 9;
             }
           }
         }
       }
     }
   }
   for (prom_for3=0; prom_for3 < EEPROM_in; prom_for3 = prom_for3+34) {
               if (EEPROM.read(prom_for3) == buffer[5]) {
                 if (EEPROM.read(prom_for3 + 1) == buffer[6]) {
                   if (EEPROM.read(prom_for3 + 2) == buffer[7]) {
                     
                     for (prom_for1=0; prom_for1<31; prom_for1++) {  // zpracování ŘD a SV
                       adrSVRD = i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4));
                       if (rd_p==true && rd_c==prom_for1+1) {
                         rd[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)));
                         bitWrite(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)), !rd[prom_for1]);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4), adrSVRD);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7, 192 | (i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7))  ); // zapsání 8a7 bite 8 bajtu log 1 - do měsíce se zapisovalo
                         rd_p=false;
                       }
                       if (sv_p==true && sv_c==prom_for1+1) {
                         sv[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1 );
                         bitWrite(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1, !sv[prom_for1]);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + (prom_for1/4), adrSVRD);
                         i2c_eeprom_write_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7, 192 | (i2c_eeprom_read_byte(adr_32, ((prom_for3/34)*96) + ((aca-1)*8) + 7))  ); // zapsání 8a7 bite 8 bajtu log 1 - do měsíce se zapisovalo
                         sv_p=false;
                       }
                       rd[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)));
                       sv[prom_for1] = bitRead(adrSVRD, 2 * (prom_for1-((prom_for1/4)*4)) + 1 );
                     }
                                          
                     for (prom_for2 = 0; prom_for2 < 20; prom_for2++) {
                       value = EEPROM.read(prom_for3 + prom_for2 + 3);
                       if (value<65 || value>122 || (value>90 && value<97)) {
                          client.write(" ");
                        }
                       else {
                         client.write(value);
                        }
                     }
                     client.print("c. ");
                     client.write(buffer[5]);
                     client.write(buffer[6]);
                     client.write(buffer[7]);
                   }
                 }
               }
             }
    client.print(";Mesic - ");
    client.print(buffer[9]);
    client.print(buffer[10]);
    client.print("/");
    client.println(rok+2000);
    client.println(F("Den;Den v tydnu;Prichod;Odchod;Prichod;Odchod;Prichod;Odchod;Hodin;Rd;Sv;Prescas"));
    
    for (prom_for1 = 1; prom_for1 < 32; prom_for1++) { 
               client.print(prom_for1);
               client.print(";");
               pom3 = 0;
              // den v týdnu a časy  ------------------------------------------            
               pom1 = 1;
               for (prom_for2 = 2048; prom_for2 < adresaEEPROM; prom_for2 = prom_for2+9) {
                 pom2 = EEPROM.read(prom_for2+3);
                 if (pom2 == prom_for1 && (pom1>2 && pom1<8)) {
                   pom1++;
                   hodiny[pom3] = EEPROM.read(prom_for2+7);
                   /*if (hodiny[pom3]<10) client.print("0");
                   client.print(hodiny[pom3]);
                   client.print(":");*/
                   minuty[pom3] = EEPROM.read(prom_for2+8);
                   /*if (minuty[pom3]<10) client.print("0");
                   client.print(minuty[pom3]);
                   client.print(";"); */
                   pom3++;
                 }
                 else if (pom2 == prom_for1 && pom1 == 1) {
                   pom1=pom1+2;
                   aca = EEPROM.read(prom_for2+6);
                   if (aca==1) den_v_tydnu = "Pondeli";
                   if (aca==2) den_v_tydnu = "Utery";
                   if (aca==3) den_v_tydnu = "Streda";
                   if (aca==4) den_v_tydnu = "Ctvrtek";
                   if (aca==5) den_v_tydnu = "Patek";
                   if (aca==6) den_v_tydnu = "Sobota";
                   if (aca==7) den_v_tydnu = "Nedele";
                   if (aca==0) den_v_tydnu = " ";
                   client.print(den_v_tydnu);
                   client.print(";");
                   hodiny[pom3] = EEPROM.read(prom_for2+7);
                   /*if (hodiny[pom3]<10) client.print("0");
                   client.print(hodiny[pom3]);
                   client.print(":");*/
                   minuty[pom3] = EEPROM.read(prom_for2+8);
                   /*if (minuty[pom3]<10) client.print("0");
                   client.print(minuty[pom3]);
                   client.print(";");*/
                   pom3++;
                   }
               }
               if (pom1 == 1) client.print(" ;");
               for (prom_for2=0; prom_for2<(8-pom1); prom_for2++) {
                 //client.print(" ;");
                 hodiny[pom3] = 0;
                 minuty[pom3] = 0;
                 pom3++;
               }
              //Součet hodin a výpočet přesčasu ---------------------------------------------------------------------
              for (pom3=0; pom3<6; pom3++) {
                if (hodiny[pom3]<6 && hodiny[pom3]>0) {
                  hodiny[pom3] = 6;
                  minuty[pom3] = 0;
                }
                if ((pom3 % 2)==0) {  
                  if (minuty[pom3]>0 && minuty[pom3]<31) {
                    minuty[pom3] = 30;
                  }
                  if (minuty[pom3]>30) {
                    minuty[pom3] = 0;
                    hodiny[pom3] = hodiny[pom3]+1;
                  }
                }
                if ((pom3 % 2)!=0) {  
                  if (minuty[pom3]>0 && minuty[pom3]<30) {
                    minuty[pom3] = 0;
                  }
                  if (minuty[pom3]>30) {
                    minuty[pom3] = 30;
                  }
                }
              //}
              if (hodiny[pom3]!=0) {
                if (hodiny[pom3]<10) client.print("0");
                client.print(hodiny[pom3]);
                client.print(":");
                if (minuty[pom3]<10) client.print("0");
                client.print(minuty[pom3]);
                client.print(";");
              }
              else {
                client.print(" ;");
              }
             }
              
              hodiny_sum[prom_for1-1]=0;
              for (pom3=0; pom3<6; pom3=pom3+2) {
                if (hodiny[pom3+1]>=hodiny[pom3]) {
                 if (minuty[pom3]==minuty[pom3+1]) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]);
                 }
                 else if ( (minuty[pom3]==30 && minuty[pom3+1]==0) && (hodiny[pom3+1]>hodiny[pom3]) ) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]-0.5);
                 }
                 else if( (minuty[pom3]==30 && minuty[pom3+1]==0) && (hodiny[pom3+1]==hodiny[pom3]) ) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + 0;
                 }
                 else if (minuty[pom3]==0 && minuty[pom3+1]==30) {
                   hodiny_sum[prom_for1-1] = hodiny_sum[prom_for1-1] + (hodiny[pom3+1]-hodiny[pom3]+0.5);
                 }
                }
               }
              if (hodiny_sum[prom_for1-1]>4.5) hodiny_sum[prom_for1-1]=hodiny_sum[prom_for1-1]-0.5;
              if (hodiny_sum[prom_for1-1]>10.5) hodiny_sum[prom_for1-1]=hodiny_sum[prom_for1-1]-0.5;
              if (hodiny_sum[prom_for1-1] != 0) {
                hodiny_sum_pom1 = (hodiny_sum[prom_for1-1]*10) / 10;
                hodiny_sum_pom2 = (hodiny_sum[prom_for1-1]*10) - (hodiny_sum_pom1*10);
                client.print(hodiny_sum_pom1);
                client.print(",");
                client.print(hodiny_sum_pom2);
              }
              else client.print(" ");
              client.print(";");
              /*for (pom3=0; pom3<11; pom3++) {
                client.print(buffer[pom3 + 5]);
              }*/
              if (rd[prom_for1-1] == 1) {  // zobrazení ŘD
                client.print("RD;");
              }
              else client.print(" ;");
              /*for (pom3=0; pom3<11; pom3++) {
                client.print(buffer[pom3 + 5]);
              }*/
              if (sv[prom_for1-1] == 1) {  // zobrazení ŘD
                client.print("SV;");
              }
              else client.print(" ;");
              if (hodiny_sum[prom_for1-1]>8 && rd[prom_for1-1]==false) {
                hodiny_sum_pom1 = ( (hodiny_sum[prom_for1-1]-8)  *10) / 10;
                hodiny_sum_pom2 = ( (hodiny_sum[prom_for1-1]-8) *10) - (hodiny_sum_pom1*10);
                client.print(hodiny_sum_pom1);
                client.print(",");
                client.print(hodiny_sum_pom2);
              }
              else if (hodiny_sum[prom_for1-1]>0 && rd[prom_for1-1]==true) {
                hodiny_sum_pom1 = (hodiny_sum[prom_for1-1]*10) / 10;
                hodiny_sum_pom2 = (hodiny_sum[prom_for1-1]*10) - (hodiny_sum_pom1*10);
                client.print(hodiny_sum_pom1);
                client.print(",");
                client.print(hodiny_sum_pom2);
              }
              else client.print(" ");
              client.println();
             }
    
    client.println(F("Odprac. dnu:;Stand. hodin:; ; ; ; ; ; ;Suma:;Rd:;Sv:;Prescas:"));
    
    
             odprac_dnu=0;
             hodiny_sum2=0;
             for (prom_for1=0; prom_for1<31; prom_for1++) {
               hodiny_sum2 = hodiny_sum2 + hodiny_sum[prom_for1];
               if (hodiny_sum[prom_for1] != 0 && rd[prom_for1]==false) odprac_dnu++;
             }
             client.print(odprac_dnu);
             client.print(";");
             client.print(odprac_dnu*8);
             client.print("; ; ; ; ; ; ;");
             hodiny_sum_pom1 = (hodiny_sum2*10) / 10;
             hodiny_sum_pom2 = (hodiny_sum2*10) - (hodiny_sum_pom1*10);
             client.print(hodiny_sum_pom1);
             client.print(",");
             client.print(hodiny_sum_pom2);
             //client.print(hodiny_sum2,1);
             client.print(";");
             rd_sum=0;
             sv_sum=0;
             for (prom_for1=0; prom_for1<31; prom_for1++) {
               rd_sum = rd_sum + rd[prom_for1];
               sv_sum = sv_sum + sv[prom_for1];
             }
             client.print(rd_sum);
             client.print(";");
             client.print(sv_sum);
             client.print(";");
             
             hodiny_sum_pom1 = ( (hodiny_sum2-(8*odprac_dnu)) *10) / 10;
             hodiny_sum_pom2 = abs (( (hodiny_sum2-(8*odprac_dnu)) *10) - (hodiny_sum_pom1*10));
             if (hodiny_sum_pom1==0 && (hodiny_sum2-(8*odprac_dnu))<0 ) client.print("-");
             client.print(hodiny_sum_pom1);
             client.print(",");
             client.print(hodiny_sum_pom2);
             //client.print( (hodiny_sum2-(8*odprac_dnu)) ,1);
             client.println();
                 
   // ****************************************************
}



// --- základní stránka ----------------------------------------
  if (podminka1 == true) {
    client.println(HTML_start); // úvodní seqence
    if(buffer.indexOf("date")>=0) { // zpracování nastavení času a data
     if ( buffer[11]>47 && buffer[11]<58 && buffer[13]>47 && buffer[13]<58 && buffer[14]>47 && buffer[14]<58 && buffer[16]>47 && buffer[16]<58 && buffer[17]>47 && buffer[17]<58 && buffer[21]>47 && buffer[21]<58 && buffer[22]>47 && buffer[22]<58 && buffer[24]>47 && buffer[24]<58 && buffer[25]>47 && buffer[25]<58 && buffer[27]>47 && buffer[27]<58 && buffer[28]>47 && buffer[28]<58 && buffer[30]>47 && buffer[30]<58 && buffer[31]>47 && buffer[31]<58) {
      s_den7 = (buffer[11]-'0');
      if (s_den7 == 0) s_den7 = 7;
      s_den = (buffer[13]-'0')*10 + (buffer[14]-'0'); // den
      s_mesic = (buffer[16]-'0')*10 + (buffer[17]-'0'); // měsíc
      s_rok = (buffer[21]-'0')*10 + (buffer[22]-'0'); // rok
      s_hod = (buffer[24]-'0')*10 + (buffer[25]-'0'); // hodin
      s_min = (buffer[27]-'0')*10 + (buffer[28]-'0'); // minut
      s_sec = (buffer[30]-'0')*10 + (buffer[31]-'0'); // vteřin
      RTC.stop(); // zastaví čas
      if (s_den7>0 && s_den7<8 && s_den7!=rtc[3]) RTC.set(DS1307_DOW, s_den7); // nastaví den v týdnu
      if (s_den>0 && s_den<32 && s_den!=rtc[4]) RTC.set(DS1307_DATE, s_den); // nastaví den v měsíci
      if (s_mesic>0 && s_mesic<13 && s_mesic!=rtc[5]) RTC.set(DS1307_MTH, s_mesic); // nastaví měsíc
      if (s_rok>=0 && s_rok<100 && s_rok!=rtc[6]) RTC.set(DS1307_YR, s_rok); // nastaví rok
      if (s_hod>=0 && s_hod<24 && s_hod!=rtc[2]) RTC.set(DS1307_HR, s_hod); // nastaví hodiny
      if (s_min>=0 && s_min<61 && s_min!=rtc[1]) RTC.set(DS1307_MIN, s_min);
      if (s_sec>=0 && s_sec<61 && s_sec!=rtc[0]) RTC.set(DS1307_SEC, s_sec);
      RTC.start(); // spustí čas
     }
    }
    client.print(F("<form action='javascript: date();' method='get' onsubmit='return passWord();'>&nbsp;<b>Datum:&nbsp;</b>"));
    client.print(datum);
    client.print(F("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
    client.println(F("<input type='submit' value='Nastavení času z PC do Docházky' title='Předtím synchronizuj čas PC s internetem'></form><br>"));
    client.println(F("<table align='left' border='1' cellpadding='6' cellspacing='0'>"));
    client.println(F("<tr><th bgcolor='red'>Editace</th><th bgcolor='red'></th><th bgcolor='red'></th><th bgcolor='red'></th><th bgcolor='green'>Prohlížení docházky</th><th bgcolor='green'></th><th bgcolor='green'></th></tr>"));
    client.println(F("<tr bgcolor='yellow'><th>Pořadové číslo</th><th>Číslo pracovníka</th><th>Jméno</th><th>Kód čipu</th><th>Aktuální měsíc</th><th>Minulý</th><th>Předminulý</th></tr>"));
    aca = rtc[5] + 1; // příprava měsíců pro výmaz ŘD a SV
    if (aca>12) aca = aca-12;
    aca2 = rtc[5] + 2;
    if (aca2>12) aca2 = aca2-12;
    
    for (prom_for1=0; prom_for1 < EEPROM_in; prom_for1=prom_for1+34){
      
      if ( (i2c_eeprom_read_byte(adr_32, ((prom_for1/34)*96) + ((aca-1)*8) + 7)) > 191 ) {  // mazání paměti ŘD a SV
        for (prom_for2=0; prom_for2<8; prom_for2++) { 
          i2c_eeprom_write_byte(adr_32, ((prom_for1/34)*96) + ((aca-1)*8) + prom_for2, 0);
        }
      }
      if ( (i2c_eeprom_read_byte(adr_32, ((prom_for1/34)*96) + ((aca2-1)*8) + 7)) > 191 ) {  // mazání paměti ŘD a SV
        for (prom_for2=0; prom_for2<8; prom_for2++) { 
          i2c_eeprom_write_byte(adr_32, ((prom_for1/34)*96) + ((aca2-1)*8) + prom_for2, 0);
        }
      }
    pom4 = true;  
    char prevod02[2];
    sprintf(prevod02, "%02d", (prom_for1/34)+1);
    client.print("<tr><td>");
    client.print(prevod02);
    client.print(".</td>");
    client.print("<td><a href='adr=");
    sprintf(prevod, "%03d", prom_for1);
    client.print(prevod);
    client.print("+val=1");
    client.print("'>");
    for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
        pom4 = false;
       }
       else {
        client.write(value);
       }
      }
    client.print("</a></td>"); 
    
    client.print("<td><a href=\"adr=");
    sprintf(prevod, "%03d", prom_for1);
    client.print(prevod);
    client.print("+val=2");
    client.print("\">");
    for (prom_for2=3; prom_for2 <=22; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<65 || value>122 || (value>90 && value<97)) {
        client.write("&nbsp;");
       }
       else {
        client.write(value);
       }
    }
   client.print("</a></td>");
      
   client.print("<td><a href=\"adr=");
   sprintf(prevod, "%03d", prom_for1);
   client.print(prevod);
   client.print("+val=3");
   client.print("\">");
   for (prom_for2=23; prom_for2 <=32; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      //sprintf(prevod2, "%02X", value); // výstup jako hex. číslo
      if (value<48 || value>57) {
        client.write("&nbsp;");
       }
       else {
        client.write(value);
       }
   }
   client.print("</a></td>");
// ---- soubory 1 ------------------------------------------------      
   client.print(("<td>"));
  if (pom4 == true) {
   client.print(("<a href='"));
   for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] < 10) client.print("0");
    client.print(rtc[5]);
    client.print((".csv'>stáhnout</a>&nbsp;&#92;&nbsp;"));
   client.print(("<a href='"));
   for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] < 10) client.print("0");
    client.print(rtc[5]);
    client.print((".html'>zobrazit</a>"));
  }
    client.print(("</td>"));
    
    // soubor 2 ---------------------------------------    
    client.print(("<td>"));
   if (pom4 == true) {
    client.print(("<a href='"));
    for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] == 1) {
      client.print(12);
    }
    else {
      if (rtc[5] < 11) client.print("0");
      client.print(rtc[5]-1);
    }
    client.print((".csv'>stáhnout</a>&nbsp;&#92;&nbsp;"));
    client.print(("<a href='"));
    for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] == 1) {
      client.print(12);
    }
    else {
      if (rtc[5] < 11) client.print("0");
      client.print(rtc[5]-1);
    }
    client.print((".html'>zobrazit</a>"));
   }
    client.print(("</td>"));
    
// soubor 3 -----------------------------------------------        
    client.print(("<td>"));
   if (pom4 == true) {
    client.print(("<a href='"));
    for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] < 3) {
      client.print(rtc[5] + 12 - 2);
    }
    else {
      if (rtc[5] < 12) client.print("0");
      client.print(rtc[5]-2);
    }
    client.print((".csv'>stáhnout</a>&nbsp;&#92;&nbsp;"));
    client.print(("<a href='"));
    for (prom_for2=0; prom_for2<=2; prom_for2++) {
      address = prom_for1 + prom_for2;
      value = EEPROM.read(address);
      if (value<48 || value>57) {
        client.write("*");
       }
       else {
        client.write(value);
       }
      }
    client.print("_");
    if (rtc[5] < 3) {
      client.print(rtc[5] + 12 - 2);
    }
    else {
      if (rtc[5] < 12) client.print("0");
      client.print(rtc[5]-2);
    }
    client.print((".html'>zobrazit</a>"));
   }
    client.print(("</td>"));   
    
    client.println("</tr>");
   }
    client.println(("</table>"));
  } 
  

// -----------------------------------------------------------   
   
// --- ukončení stránky a spojení ------
     if (csv == false) client.println(HTML_konec);
     csv = false;
     delay(5);
     client.stop();  //ukončí přenos
     }
    }
   } 
}

